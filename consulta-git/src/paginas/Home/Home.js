import React from 'react'
import './Home.css';
import logo from "../../imagens/logoShadow.svg"

function Home() {

  return (
    <>     
      <header className="Home-header">        
        <img src={logo} width="53.677%" height="30.147%"/>
      </header>
    </>
  );
}

export default Home;
