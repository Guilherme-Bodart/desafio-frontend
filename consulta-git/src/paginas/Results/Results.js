import React from 'react'
import { useSelector } from 'react-redux';

import CardS from "../../componentes/Card/Card"
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'

import './Results.css';

function Results() {
 
  const listaU = useSelector(state => state.result.listaUsuarios)  

  var cards = listaU.map( (card, index) => 
    {     
      if(index<4){
        return(
          <Col style={{marginTop:"5em", marginBottom:"2em"}} md="3">{<CardS pessoa={card}/>}</Col>
          
        )
      }
      else{
        if(index<8){
          return(
            <Col style={{marginBottom:"2em"}} md="3">{<CardS pessoa={card}/>}</Col>
            
          )
        }
      }
    }
  )
  return (
    <div className="Results-header">
      <Container md="12" >
        <Row md="12" style={{marginLeft:"2em"}}>
          {cards}
        </Row>
      </Container>
      
    </div>
    
  );
}

export default Results