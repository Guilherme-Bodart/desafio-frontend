import React from 'react'
import { useSelector } from 'react-redux';

import Home from './paginas/Home/Home'
import Results from './paginas/Results/Results'
import NavbarS from './componentes/Navbar/Navbar'
import Footer from './componentes/Footer/Footer'

function App() {
  const page = useSelector(state => state.page.page)
  let view =  page === 'home' ? <Home/> :
              page === 'result' ? <Results/> : 
              <Home/>
  return (
    <div >
      <header className="App-header">
        <NavbarS/>
        {view}
        <Footer/>

      </header>
    </div>
  );
}

export default App;

