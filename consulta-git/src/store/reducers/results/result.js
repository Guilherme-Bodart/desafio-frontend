import { LISTA_USUARIOS, GET_USUARIO, MODAL_SHOW } from "../../actions/actionsTypes"

const initialState = {
    listaUsuarios: [],
    usuarioModal: {},
    modalShow: false,
}

const reducer = (state = initialState, action) => {
    switch (action.type) {  

        case LISTA_USUARIOS:
            let listaUsuarios = action.payload
            return {                
                ...state, listaUsuarios
            }
        
        case GET_USUARIO:
            let usuarioModal = action.payload
            return {                
                ...state, usuarioModal
            }
        

        case MODAL_SHOW:
            
            let modalShow = action.payload
            return {                
                ...state, modalShow
            }
        
        
        default:
            return state
            }       
}

export default reducer