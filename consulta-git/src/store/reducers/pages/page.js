import { PAGE_RESULT, PAGE_HOME } from '../../actions/actionsTypes'

const initialState = {
    page: 'home',
}

const reducer = (state = initialState, action) => {

switch (action.type) {

   case PAGE_HOME:
       var page = 'home'

       return {
           ...state, page
       }

   case PAGE_RESULT:
        page = 'result'
        return {
            ...state, page
        }
   
   default:
       return state
}
}

export default reducer