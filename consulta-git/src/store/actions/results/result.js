import { GET_USUARIO, LISTA_USUARIOS, MODAL_SHOW } from '../actionsTypes'

import axios from 'axios'

import { pageHome, pageResult } from '../pages/page'

export const getUsuarios = (usuario) => {
    return (dispatch) => {
       if(usuario!==''){
        axios({
            method: 'get',
            url: 'https://api.github.com/search/users?q='+ usuario,
        })
            .then(function (response) {
                let usuarios = response.data.items
                dispatch(pageResult())
                dispatch(armazenaListaUsuarios(usuarios))
            });
        }
        else{
            dispatch(pageHome())
            dispatch(armazenaListaUsuarios([]))
        }
    }
}

export const getUsuario = (usuario) => {
    return (dispatch) => {
        if(usuario!==''){
            axios({
                method: 'get',
                url: 'https://api.github.com/users/'+ usuario,
            })
                .then(function (response) {
                    let usuario = response.data
                    dispatch(armazenaUsuario(usuario))
                });
            }
            else{
                dispatch(armazenaUsuario([]))
            }
    }
}

export const armazenaListaUsuarios = usuarios => {
    return { 
        type: LISTA_USUARIOS, 
        payload: usuarios,
    }
}

export const armazenaUsuario = usuario => {
    return { 
        type: GET_USUARIO, 
        payload: usuario,
    }
}

export const modalShow = show => {
    return { 
        type: MODAL_SHOW, 
        payload: show,
    }
}