import { PAGE_HOME, PAGE_RESULT } from '../actionsTypes'

export const pageHome = () => {
    return {
        type: PAGE_HOME
    }
}

export const pageResult = () => {
    return {
        type: PAGE_RESULT
    }
}