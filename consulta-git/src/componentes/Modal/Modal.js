import React, {useState} from 'react'
import { connect } from 'react-redux';

import Modal from 'react-bootstrap/Modal'
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'
import Image from 'react-bootstrap/Image'


function ModalS(props) {  
  
    const [search, setSearch] = useState('')
    var nome = props.pessoa.name
    var git = props.pessoa.html_url
    var seguidores = props.pessoa.followers
    var seguindo = props.pessoa.following
    var cadastrado = props.pessoa.created_at.slice(0,10).split('-').reverse().join('/')
    var avatar = props.pessoa.avatar_url
    return (
      <Modal {...props} size="lg" centered >
        <Modal.Body >
        <Container >
         <Row className="modalRowPrincipal">
          <Col md="5">
            <Image className="modalFoto" src={avatar} />
          </Col>
          <Col >
            <Row style={{fontSize:"18pt", fontWeight: 400, borderBottom: "1px solid #dee2e6" }}>
              {nome}
            </Row>
            <br/>
            <Row className= "modalRowInfo">
            <Col>
            
              <Row>Username:<br/>{nome}</Row><br/>
              <Row>Cadastrado(a):<br/>{cadastrado}</Row><br/>
              <Row>URL:<br/>{git}</Row><br/>
            </Col>
            <Col>
              <Row className="modalRowInfoDireita" >Seguindo:<br/> {seguindo}</Row><br/>
              <Row className="modalRowInfoDireita" >Seguidores:<br/>{seguidores}</Row><br/>
            </Col>
            </Row>
          <Row >
            <Col md={{span: 3, offset: 9}}>
              <button className="buttonOutlinePrimary buttonModal" onClick={props.onHide}>Fechar</button>
            </Col>
          </Row>
          </Col>
          </Row>
        </Container>
        
        </Modal.Body>
          
      </Modal>
    );
  }


export default ModalS