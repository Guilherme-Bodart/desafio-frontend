import React, {Component} from 'react'
import { connect } from 'react-redux';
import Navbar from 'react-bootstrap/Navbar'
import Form from 'react-bootstrap/Form'
import FormControl from 'react-bootstrap/FormControl'
import Image from 'react-bootstrap/Image'

import { BsSearch } from 'react-icons/bs';

import logo from "../../imagens/logoNavbar.svg"
import { getUsuarios } from '../../store/actions/results/result'
import { pageHome } from '../../store/actions/pages/page'

const initialState = {
  search: '',
}
class NavbarS extends Component {  
  constructor(props) {
    super(props)
    this.state = initialState
  }

  onChangeSearch = (value) => {
    this.setState({
      search: value
    })
  }
  render() { 
    
    return (
      <div className="App-navbar-div">
          <Navbar className="App-navbar" >
            <Navbar.Brand href="/" onClick={() => {this.props.pageHome()}}><Image src={logo} className="App-logo"/></Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav" style={{flexDirection: "column", alignItems: "stretch", marginRight: "1.5em", marginLeft: "1em"}}>

              <Form className="d-flex">
              <FormControl
                type="procurar"
                placeholder="Procurar"
                className="mr-2"
                aria-label="Procurar"
                style={{borderRadius:0}}
                onChange = {event => this.onChangeSearch(event.target.value)}
                
              />
              
              <button type="button" className="buttonPrimary buttonSearch" onClick={ async () =>{ this.props.getUsuarios(this.state.search)} }> 
              <BsSearch style={{height:"20px", width:"20px"}}/> </button>
              
            </Form>
            </Navbar.Collapse>
          </Navbar>

      </div>
    );

  }
}
const mapStateToProps = ({ result, page }) => {
  return {
      result,
      page, 
  }
}

const mapDispatchToProps = dispatch => {
  return {
      getUsuarios: usuario => dispatch(getUsuarios(usuario)),     
      pageHome: () => dispatch(pageHome()),     
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(NavbarS)