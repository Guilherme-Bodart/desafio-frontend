import React, {Component} from 'react'
import { connect } from 'react-redux';

import Card from 'react-bootstrap/Card'
import ModalS from '../Modal/Modal'

import { getUsuario } from '../../store/actions/results/result'

const initialState = {
    modal: false,
  }

class CardS extends Component {  
    constructor(props) {
      super(props)
      this.state = initialState
    }  

    onChangeModal = (value) => {
        this.setState({
          modal: value
        })
    }

    componentDidMount = () => {
        
    }


    render(props) {
        
        var git = this.props.pessoa.html_url
        var score = this.props.pessoa.score
        var avatar = this.props.pessoa.avatar_url
        var nome = this.props.pessoa.login
        return (
            <>
            <Card className="card-Principal">
            <Card.Img variant="top" className="card-Img" src={avatar} />
            <Card.Body className="card-Body">
            <Card.Title className="card-Title">{nome}</Card.Title>
            <Card.Text className="card-Text">
            <a href={git} className="card-Href" >{git}</a><br/>
            Score: {score}
            </Card.Text>
            <button className="buttonPrimary buttonCard" onClick={() => {   this.props.getUsuario(this.props.pessoa.login)
                                                                            this.onChangeModal(true)}
                }>VER MAIS</button>
            </Card.Body>
        </Card>
        <ModalS show={this.state.modal} pessoa={this.props.result.usuarioModal} onHide={() => this.onChangeModal(false)}/>
        </>
        );
    }
  }
  const mapStateToProps = ({ result, page }) => {
    return {
        result,
        page, 
    }
  }
  
  const mapDispatchToProps = dispatch => {
    return {
        getUsuario: usuario => dispatch(getUsuario(usuario)),   
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(CardS)